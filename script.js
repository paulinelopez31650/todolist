// Créer un bouton "fermer" et l'ajouter à chaque élément de la liste

var myNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) 
{
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7"); //Cette méthode crée un nœud de type élément HTML.
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}


// Cliquez sur un bouton de fermeture pour masquer l'élément de liste actuel

var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) 
{
  close[i].onclick = function() 
  {
    var div = this.parentElement;
    div.style.display = "none";
  }
}

// Ajout d'un symbole "coché" en cliquant sur un élément de la liste

var list = document.querySelector('ul'); // comme getelementby ...
list.addEventListener('click', function(ev) // addEventListener() installe une fonction à appeler chaque fois que l'événement spécifié est envoyé à la cible
{ 
  if (ev.target.tagName === 'LI') 
  {
    ev.target.classList.toggle('checked');
  }
}, false);

// Créer un nouvel élément de liste en cliquant sur le bouton "Ajouter"

function nouvelleTache() 
{
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  if (inputValue === '') {
    alert("Tu dois écrire un truc!");
  } else {
    document.getElementById("myUL").appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (i = 0; i < close.length; i++) 
  {
    close[i].onclick = function() 
    {
      var div = this.parentElement;
      div.style.display = "none";
    }
  }
} 

// Créer une fonction pour afficher les taches réalisées quand on clique sur le bouton
// function masquerTache()

// let cocher = document.getElementsByClassName("checked")
// {
//   if (li != cocher)
//   {
//     document.querySelector(myUL).style.display = none;
//   }
//   else
//   {
//     li.document.getElementById(myUL).style.display = block;
//   }

// }

// document.addEventListener("keyup",function(even)
// {
//   if(event.keyCode == 13)
//   {
//       const toDo = input.value;

//       // if the input isn't empty
//       if(toDo)
//       {
//           addToDo(toDo, id, false, false);

//           LIST.push(
//             {
//               name : toDo,
//               id : id,
//               done : false,
//               trash : false
//             });

//           // add item to localstorage ( this code must be added where the LIST array is updated)
//           localStorage.setItem("TODO", JSON.stringify(LIST));

//           id++;
//       }
//       input.value = "";
//   }
// });
